window.addEventListener("load", function ()
{
    var div_hover = false;
    var nav_details = false;
    var curr_details = false;
    var body = document.getElementsByTagName("body")[0];
    function updateNavStatus()
    {
        var tmp = div_hover || nav_details;
        if(tmp != curr_details)
        {
            curr_details = tmp;
            body.classList[tmp ? "add" : "remove"]("details");
        }
    }
    function div_over()
    {
        div_hover = true;
        updateNavStatus();
    }
    function div_out()
    {
        div_hover = false;
        updateNavStatus();
    }
    document.getElementById("details").addEventListener("click", function ()
    {
        nav_details = !nav_details;
        updateNavStatus();
    });
    document.querySelector("body > div").addEventListener("mouseover", div_over);
    document.querySelector("body > div").addEventListener("mouseout", div_out);
    var curr_active;
    function getOnClick(elem)
    {
        return function ()
        {
            curr_active.classList.remove("active");
            elem.classList.add("active");
            curr_active = elem;
        };
    }
    var elem = document.querySelectorAll("nav a");
    curr_active = elem[0];
    for(var i = 0; i < elem.length; i++)
    {
        elem[i].addEventListener("click", getOnClick(elem[i]));
    }
});
